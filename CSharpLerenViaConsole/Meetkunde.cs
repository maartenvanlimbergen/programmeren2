using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpLerenViaConsole.Meetkunde
{
    public struct Point
    {
        //private= alleen zichtbaar in de struct
        //x en y readonly, ze mogen gelezen worden maar niet gewijzigd worden
        public readonly int x;
        public readonly int y;

       public Point(int x,int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
