﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog
{
    class Persoon
    {
        public string voornaam;
        public string familienaam;
        private int leeftijd;

        public int Leeftijd
        {
            get { return leeftijd; }
            set
            {
                if (value >= 0 && value <= 120)
                {
                    leeftijd = value;
                }
                else
                {
                    Console.WriteLine($"{value} is geen geldige leeftijd! Lefetijd tussen 0 en 120");
                }
            }
        }

        public string ShowInfo()
        {
            return $"Voornaam: {voornaam}\nFamilienaam: {familienaam}\n leeftijd: {leeftijd}";
        }
    }
}
