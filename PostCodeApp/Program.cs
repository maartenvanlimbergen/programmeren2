﻿using System;
﻿using Helpers;

namespace PostcodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("De Postcode app");
            Tekstbestand postcodeCSV = new Tekstbestand("Data/postcodes.csv");
            postcodeCSV.Lees();
            Console.WriteLine(postcodeCSV.Text);
            Console.WriteLine(postcodeCSV.Melding);
        }
    }
}
